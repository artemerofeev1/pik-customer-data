<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->name,
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->unique()->phoneNumber,
            'last_name' => $this->faker->lastName,
            'password' => app('hash')->make('password'),
        ];
    }
}
